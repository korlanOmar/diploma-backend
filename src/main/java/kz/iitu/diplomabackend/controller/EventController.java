package kz.iitu.diplomabackend.controller;

import kz.iitu.diplomabackend.model.Event;
import kz.iitu.diplomabackend.model.ScheduleModel;
import kz.iitu.diplomabackend.service.EventService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/events")
public class EventController {
    @Autowired
    private EventService eventService;

    @GetMapping
    public List<Event> getAllTeamMembers() {
        return eventService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<Event> findEventById(@PathVariable Long id) {
       return eventService.findEventById(id);
    }

    @PostMapping("/create")
    public void createEvent(@RequestParam String name, @RequestParam String time) throws ParseException {
//      07-09-2013 01:02:32
        System.out.println("/// " + name);
        Date date = DateUtils.parseDate(time, "yyyy-MM-dd HH:mm:ss");
        Timestamp ts = new Timestamp(date.getTime());
        Event event = new Event();
        event.setTime(ts);
        event.setName(name);
        System.out.println("/// " + event.getName());
        eventService.createEvent(event);
    }

    @PutMapping("/edit")
    public void updateEvent(@RequestParam Long id, @RequestParam String name, @RequestParam String time) throws ParseException {
        Date date = DateUtils.parseDate(time, "yyyy-MM-dd HH:mm:ss");
        Timestamp ts = new Timestamp(date.getTime());
        Event event = new Event();
        event.setId(id);
        event.setTime(ts);
        event.setName(name);
        eventService.updateEvent(event);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteEvent(@PathVariable Long id) {
        eventService.deleteEvent(id);
    }
}
