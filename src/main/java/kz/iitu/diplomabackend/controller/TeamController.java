package kz.iitu.diplomabackend.controller;

import kz.iitu.diplomabackend.model.Team;
import kz.iitu.diplomabackend.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/teams")
public class TeamController {
    @Autowired
    private TeamService teamService;

    @GetMapping
    public List<Team> getAllTeams() {
        return teamService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<Team> findTeamById(@PathVariable Long id) {
        return teamService.findTeamById(id);
    }

    @PostMapping("/create")
    public void createTeam(@RequestBody Team team) {
        teamService.createTeam(team);
    }

    @PutMapping("/edit")
    public void updateTeam(@RequestBody Team team) {
        teamService.updateTeam(team);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteTeam(@PathVariable Long id) {
        teamService.deleteTeam(id);
    }
}
