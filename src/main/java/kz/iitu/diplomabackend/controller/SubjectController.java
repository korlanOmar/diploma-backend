package kz.iitu.diplomabackend.controller;

import kz.iitu.diplomabackend.model.*;
import kz.iitu.diplomabackend.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.*;

@RestController
@RequestMapping("/subjects")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    @GetMapping
    public List<Subject> getAllTeamMembers() {
        return subjectService.getAll();
    }

    @GetMapping("/{id}")
    public Subject findSubjectById(@PathVariable Long id) {
        return subjectService.findSubjectById(id);
    }

    @PostMapping("/create")
    public void createSubject(@RequestBody Subject subject) {
        subjectService.createSubject(subject.getEventId(), subject.getTeamId(), subject.getStatus());
    }

    @PutMapping("/edit/status/{id}")
    public void updateSubject(@PathVariable Long id, @RequestBody Subject subject) {
        Subject newSubject = subjectService.findSubjectById(id);
        if (subject.getStatus() != null) {
            newSubject.setStatus(subject.getStatus());
        }
        subjectService.updateSubject(newSubject.getId(), newSubject.getEventId(), newSubject.getTeamId(), newSubject.getStatus());
    }

    @GetMapping("/teamId/{id}")
    public List<Subject> findByTeamId(@PathVariable Long id) {
        List<Subject> subjectList = subjectService.findByTeamId(id);
        List<Subject> subjects  = new ArrayList<>();
        List<Long> eventIds = new ArrayList<>();
        for (Subject subject: subjectList) {
            if (!eventIds.contains(subject.getEventId())) {
                subjects.add(subject);
                eventIds.add(subject.getEventId());
            }
        }
        return subjects;
    }

    @GetMapping("/futureSubjects/{teamId}")
    public List<Event> getFutureDefences(@PathVariable Long teamId) {
        List<Subject> subjects = subjectService.findByTeamId(teamId);
        List<Event> events = new ArrayList<>();
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        for (Subject subject: subjects) {
            Event event = subject.getEvent();
            int compareResult = ts.compareTo(event.getTime());
            if (compareResult < 0 && !events.contains(event)) {
                events.add(event);
            }
        }
        return events;
    }

    @GetMapping("/mySubjects/{teamId}")
    public Map<String, String> getSubjectsByTeamId(@PathVariable Long teamId){
        List<Subject> subjectList = subjectService.getAll();
        Map<String, String> mySubjects = new HashMap<>();
        for (Subject subject: subjectList) {
            if (subject.getTeamId().equals(teamId)) {
                String status = subject.getStatus().name();
                mySubjects.put(subject.getEvent().getName(), status);
            }
        }
        return mySubjects;
    }

    @DeleteMapping("/delete/{id}")
    public void deleteSubject(@PathVariable Long id) {
        subjectService.deleteSubject(id);
    }
}
