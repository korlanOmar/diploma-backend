package kz.iitu.diplomabackend.controller;

import kz.iitu.diplomabackend.model.*;
import kz.iitu.diplomabackend.service.PotentialTeamService;
import kz.iitu.diplomabackend.service.TeamMemberService;
import kz.iitu.diplomabackend.service.TeamService;
import kz.iitu.diplomabackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/potentialTeams")
public class PotentialTeamController {

    @Autowired
    private PotentialTeamService potentialTeamService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private TeamMemberService teamMemberService;

    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public void createPotentialTeam(@RequestBody PotentialTeam potentialTeam) {
        potentialTeam.setStatus(Status.UNDEFINED);
        potentialTeamService.createPotentialTeam(potentialTeam);
    }

    @GetMapping("/creator/{id}")
    public List<PotentialTeam> findAllByCreatorId(@PathVariable Long id) {
        return potentialTeamService.findAllByCreatorId(id);
    }

    @GetMapping
    public List<PotentialTeam> getAllUsers() {
        return potentialTeamService.getAll();
    }

    @GetMapping("/user/{id}")
    public List<PotentialTeam>  getTeamsByUserId(@PathVariable Long id) {
        return potentialTeamService.findAllByUserId(id);
    }

    @GetMapping("/{id}")
    public Optional<PotentialTeam> findPotentialTeamById(@PathVariable Long id) {
        return potentialTeamService.findById(id);
    }

    @PutMapping("/edit/{id}")
    public void updatePotentialTeamById(@PathVariable Long id, @RequestBody PotentialTeam potentialTeam) {
        Optional<PotentialTeam> team = potentialTeamService.findById(id);
        if (!team.orElseThrow().getStatus().toString().isEmpty()) {
            team.orElseThrow().setStatus(potentialTeam.getStatus());
        }

        potentialTeamService.updatePotentialTeam(team.orElseThrow());
    }

    @PostMapping("/createFinalTeam")
    public Long createFinalTeam(@RequestParam Long creatorId, @RequestParam String theme, @RequestParam String name) {
        List<PotentialTeam> finalTeamMembers = potentialTeamService.findAllByCreatorId(creatorId);
        Team team = new Team();
        team.setTheme(theme);
        team.setName(name);
        teamService.createTeam(team);
        Long teamId = teamService.getTeamByName(name).getId();

        User creator = userService.findUserById(creatorId);

        teamMemberService.createTeamMember(creatorId, teamId, creator.getRole().toString());

        for (PotentialTeam finalTeamMember: finalTeamMembers) {
            if (finalTeamMember.getStatus().equals(Status.ACCEPTED)) {
                createMember(finalTeamMember, teamId);
            }
            potentialTeamService.deletePotentialTeam(finalTeamMember.getId());
        }

        return teamId;
    }

    private void createMember(PotentialTeam finalTeamMember, Long teamId) {
        teamMemberService.createTeamMember(finalTeamMember.getUserId(), teamId, finalTeamMember.getUserRole().toString());
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUser(@PathVariable Long id) {
        potentialTeamService.deletePotentialTeam(id);
    }
}
