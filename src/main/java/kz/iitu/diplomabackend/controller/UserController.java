package kz.iitu.diplomabackend.controller;

import kz.iitu.diplomabackend.model.User;
import kz.iitu.diplomabackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    public JavaMailSender emailSender;

    @GetMapping
    public List<User> getAllUsers() {
        return userService.findAllUsers();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable Long id) {
        return userService.findUserById(id);
    }

    @GetMapping("/username")
    public List<User> getUsersByUserName(@RequestParam String username){
        return userService.getUsersByUsername(username);
    }

    @GetMapping("/position")
    public List<User> getUsersByPosition(@RequestParam String position){
        return userService.getUsersByPosition(position);
    }

    @PostMapping("/create")
    public void createUser(@RequestBody User user) {
        userService.createUser(user);
    }

    @PutMapping("/edit/{id}")
    public User updateUser(@PathVariable Long id, @RequestBody User user) {
        User newUser = userService.findUserById(id);
        if (!user.getFirstname().isEmpty()) {
            newUser.setFirstname(user.getFirstname());
        }

        if (!user.getLastname().isEmpty()) {
            newUser.setLastname(user.getLastname());
        }

        if (!user.getPhone().isEmpty()) {
            newUser.setPhone(user.getPhone());
        }

        if (!user.getGroupName().isEmpty()) {
            newUser.setGroupName(user.getGroupName());
        }

        if (!user.getDepartment().isEmpty()) {
            newUser.setDepartment(user.getDepartment());
        }

        if (!user.getPosition().isEmpty()) {
            newUser.setPosition(user.getPosition());
        }

        userService.updateUser(newUser);
        return newUser;
    }

    @PutMapping("/changePassword/{id}")
    public void updatePassword(@PathVariable Long id, @RequestParam String currentPassword, @RequestParam String newPassword){
        User newUser = userService.findUserById(id);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        boolean isPasswordMatches = encoder.matches(currentPassword, newUser.getPassword());
        if (isPasswordMatches) {
            newUser.setPassword(newPassword);
            userService.createUser(newUser);
        }
    }

    @PutMapping("/changePassword/email")
    public void updatePasswordByEmail(@RequestParam String email, @RequestParam String newPassword) {
        User user = userService.findUserByUsername(email).orElseThrow();
        user.setPassword(newPassword);
        userService.createUser(user);
    }


    @GetMapping("/forgotPassword")
    public String forgotPassword(@RequestParam String email) {
        User user = userService.findUserByUsername(email).orElseThrow();
        SimpleMailMessage message = new SimpleMailMessage();
        Random r = new Random();
        String randomNumber = String.format("%04d", (Object) r.nextInt(1001));
        if (user != null) {
            message.setTo(email);
            message.setSubject("Diploma.kz: Your SMS confirmation code to change password: ");
            System.out.println(randomNumber);
            message.setText(randomNumber);
            this.emailSender.send(message);
            return randomNumber;
        }
        return "Email not found";
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }
}

