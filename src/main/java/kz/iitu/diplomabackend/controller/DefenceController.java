package kz.iitu.diplomabackend.controller;

import kz.iitu.diplomabackend.model.Defense;
import kz.iitu.diplomabackend.model.Event;
import kz.iitu.diplomabackend.model.UserGrade;
import kz.iitu.diplomabackend.service.DefenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.*;

@RestController
@RequestMapping("/defences")
public class DefenceController {
    @Autowired
    private DefenceService defenceService;

    @GetMapping
    public List<Defense> getAllTeamMembers() {
        return defenceService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<Defense> findDefenseById(@PathVariable Long id) {
        return defenceService.findDefenceById(id);
    }

    @GetMapping("/teamId/{id}")
    public List<Defense> findByTeamId(@PathVariable Long id) {
        List<Defense> defenseList = defenceService.findByTeamId(id);
        List<Defense> defenses  = new ArrayList<>();
        List<Long> eventIds = new ArrayList<>();
        for (Defense defense: defenseList) {
            if (!eventIds.contains(defense.getEventId())) {
                defenses.add(defense);
                eventIds.add(defense.getEventId());
            }
        }
        return defenses;
    }

    @GetMapping("all/teamId/{id}")
    public Map<String, List<UserGrade>> findAllByTeamId(@PathVariable Long id) {
        List<Defense> defenseList = defenceService.findByTeamId(id);

        Map<String, List<UserGrade>> defenseUsers = new HashMap<>();

        List<String> eventNames = new ArrayList<>();
        for (Defense defense: defenseList) {
            if (!eventNames.contains(defense.getEvent().getName())) {
                eventNames.add(defense.getEvent().getName());
            }
        }

        for (String eventName: eventNames) {
            List<UserGrade> defenseUserGrades = new ArrayList<>();
            for (Defense defense : defenseList) {
                if (defense.getEvent().getName().equals(eventName)) {
                    defenseUserGrades.add(defense.getUserGrade());
                }
            }
            defenseUsers.put(eventName, defenseUserGrades);
        }
        return defenseUsers;
    }

    @GetMapping("test/teamId/{id}")
    public List<Defense> findAllTestByTeamId(@PathVariable Long id) {
        List<Defense> defenseList = defenceService.findByTeamId(id);
        List<Defense> defenses  = new ArrayList<>();

        List<String> eventNames = new ArrayList<>();
        for (Defense defense: defenseList) {
            if (!eventNames.contains(defense.getEvent().getName())) {
                eventNames.add(defense.getEvent().getName());
            }
        }

        for (String eventName: eventNames) {
            for (Defense defense : defenseList) {
                if (defense.getEvent().getName().equals(eventName)) {
                    defenses.add(defense);
                }
            }
        }
        return defenses;
    }

    @GetMapping("/futureDefences/{teamId}")
    public List<Event> getFutureDefences(@PathVariable Long teamId) {
        List<Defense> defenses = defenceService.findByTeamId(teamId);
        List<Event> events = new ArrayList<>();
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        for (Defense defense: defenses) {
            Event event = defense.getEvent();
            int compareResult = ts.compareTo(event.getTime());
            if (compareResult < 0 && !events.contains(event)) {
                events.add(event);
            }
        }
        return events;
    }
    @GetMapping("/myDefences/{id}")
    public Map<String, String> getDefencesByUserId(@PathVariable Long id){
        List<Defense> defenseList = defenceService.getAll();
        Map<String, String> myDefences = new HashMap<>();
        for (Defense defense: defenseList) {
            if (defense.getUserGrade().getUserId().equals(id)) {
                String userGrade = defense.getUserGrade().getGrade();
                myDefences.put(defense.getEvent().getName(), userGrade);
            }
        }
        return myDefences;
    }

    @PostMapping("/create")
    public void createDefense(@RequestParam Long eventId,
                              @RequestParam Long teamId,
                              @RequestParam Long userId) {
        defenceService.createDefence(eventId, teamId, userId);
    }

    @PutMapping("/edit")
    public void updateDefense(@RequestParam Long id,
                              @RequestParam Long eventId,
                              @RequestParam Long teamId,
                              @RequestParam Long userId) {
        defenceService.updateDefence(id, eventId, teamId, userId);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteDefense(@PathVariable Long id) {
        defenceService.deleteDefence(id);
    }
}
