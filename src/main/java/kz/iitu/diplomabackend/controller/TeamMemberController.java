package kz.iitu.diplomabackend.controller;

import kz.iitu.diplomabackend.model.TeamMember;
import kz.iitu.diplomabackend.service.TeamMemberService;
import kz.iitu.diplomabackend.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/teamMembers")
public class TeamMemberController {
    @Autowired
    private TeamMemberService teamMemberService;

    @Autowired
    private TeamService teamService;

    @GetMapping
    public List<TeamMember> getAllTeamMembers() {
        return teamMemberService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<TeamMember> findTeamMemberById(@PathVariable Long id) {
        return teamMemberService.findTeamMemberById(id);
    }

    @GetMapping("/team/{teamId}")
    public List<TeamMember> findByTeamId(@PathVariable Long teamId) {
        return teamMemberService.getAllByTeamId(teamId);
    }

    @PostMapping("/create")
    public void createTeamMember(@RequestParam Long userId,
                                 @RequestParam Long teamId,
                                 @RequestParam String role) {
        teamMemberService.createTeamMember(userId, teamId, role);
    }

    @PutMapping("/edit")
    public void updateTeamMember(@RequestParam Long id,
                           @RequestParam Long userId,
                           @RequestParam Long teamId,
                           @RequestParam String role) {
        teamMemberService.updateTeamMember(id, userId, teamId, role);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteTeamMember(@PathVariable Long id) {
        teamMemberService.deleteTeamMember(id);
    }

    @DeleteMapping("/deleteTeam/{teamId}")
    public void deleteTeam(@PathVariable Long teamId) {
        List<TeamMember> teamMembers = teamMemberService.getAllByTeamId(teamId);
        for (TeamMember teamMember: teamMembers) {
            teamMemberService.deleteTeamMember(teamMember.getId());
        }
        teamService.deleteTeam(teamId);
    }
}
