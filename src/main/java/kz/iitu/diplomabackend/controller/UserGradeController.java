package kz.iitu.diplomabackend.controller;

import kz.iitu.diplomabackend.model.UserGrade;
import kz.iitu.diplomabackend.service.UserGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/userGrades")
public class UserGradeController {
    @Autowired
    private UserGradeService userGradeService;

    @GetMapping
    public List<UserGrade> getAllTeamMembers() {
        return userGradeService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<UserGrade> findUserGradeById(@PathVariable Long id) {
        return userGradeService.findUserGradeById(id);
    }

    @PostMapping("/create")
    public void createUserGrade(@RequestParam Long userId, @RequestParam String grade) {
        userGradeService.createUserGrade(userId, grade);
    }

    @PutMapping("/edit")
    public void updateUserGrade(@RequestParam Long id, @RequestParam Long userId, @RequestParam String grade) {
        userGradeService.updateUserGrade(id, userId, grade);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUserGrade(@PathVariable Long id) {
        userGradeService.deleteUserGrade(id);
    }
}
