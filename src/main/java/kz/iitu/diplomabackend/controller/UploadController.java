package kz.iitu.diplomabackend.controller;

import kz.iitu.diplomabackend.model.*;
import kz.iitu.diplomabackend.service.DefenceService;
import kz.iitu.diplomabackend.service.EventService;
import kz.iitu.diplomabackend.service.SubjectService;
import kz.iitu.diplomabackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/uploadList")
public class UploadController {
    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    private UserService userService;

    @Autowired
    private EventService eventService;

    @Autowired
    private DefenceService defenceService;

    @Autowired
    private SubjectService subjectService;


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void uploadList(@RequestBody List<UserModel> users) {
        SimpleMailMessage message = new SimpleMailMessage();
        for (UserModel user: users) {
            message.setTo(user.getEmail());
            message.setSubject("You registered to Diploma.kz with email: " + user.getEmail());
            message.setText("password: 1234");
            this.emailSender.send(message);
            this.createUser(user);
        }
    }

    private void createUser(UserModel userModel){
        User user = new User();
        user.setFirstname(userModel.getName());
        user.setLastname(userModel.getSurname());
        user.setUsername(userModel.getEmail());
        user.setPosition(userModel.getRole().name());
        user.setPassword("1234");
        user.setRole(Role.USER);
        userService.createUser(user);
    }

    @PostMapping("/uploadSchedule")
    public void uploadSchedule(@RequestBody List<ScheduleModel> scheduleModel){
        for (ScheduleModel schedule: scheduleModel) {
            createEvent(schedule);
        }
    }

    private void createEvent(ScheduleModel schedule) {
        Event event = new Event();
        event.setName(schedule.getEventName());
        event.setTime(schedule.getTime());
        event.setUserId(schedule.getTeacherId());
        eventService.createEvent(event);
        Event createdEvent = eventService.findEventByNameAndTime(event.getName(), event.getTime());
        if (schedule.isDefense()) {
            createDefense(schedule, createdEvent.getId());
        } else {
            createSubject(schedule, createdEvent.getId());
        }
    }

    private void createDefense(ScheduleModel schedule, Long id) {
        Defense defense = new Defense();
        defense.setEventId(id);
        defense.setTeamId(schedule.getTeamId());
        defenceService.createDefence(defense.getEventId(), defense.getTeamId(), null);
    }

    private void createSubject(ScheduleModel schedule, Long id) {
        Subject subject = new Subject();
        subject.setEventId(id);
        subject.setTeamId(schedule.getTeamId());
        subject.setStatus(SubjectStatus.UNDEFINED);
        subjectService.createSubject(subject.getEventId(), subject.getTeamId(), subject.getStatus());
    }
}
