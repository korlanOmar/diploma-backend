package kz.iitu.diplomabackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table(name = "_subject")
public class Subject {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "event_id",nullable = false,insertable = false,updatable = false)
  private Event event;
  @Column(name = "event_id")
  private Long eventId;

  @ManyToOne
  @JsonIgnore
  @JoinColumn(name = "team_id",nullable = false,insertable = false,updatable = false)
  private Team team;
  @Column(name = "team_id")
  private Long teamId;

  @Enumerated(EnumType.STRING)
  private SubjectStatus status;
}
