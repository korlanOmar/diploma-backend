package kz.iitu.diplomabackend.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "_team")
public class Team {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  private String theme;
}
