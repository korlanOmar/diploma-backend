package kz.iitu.diplomabackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "_defense")
public class Defense {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "event_id",nullable = false,insertable = false,updatable = false)
  private Event event;
  @Column(name = "event_id")
  private Long eventId;

  @ManyToOne
  @JoinColumn(name = "team_id",nullable = false,insertable = false,updatable = false)
  private Team team;
  @Column(name = "team_id")
  private Long teamId;

  @ManyToOne
  @JoinColumn(name = "grade_id",nullable = false,insertable = false,updatable = false)
  private UserGrade userGrade;
  @Column(name = "grade_id")
  private Long userGradeId;

}
