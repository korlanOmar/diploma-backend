package kz.iitu.diplomabackend.model;

import lombok.ToString;

@ToString
public enum TeamMemberRole {
  STUDENT,SV,COMMISSION,TEACHER
}
