package kz.iitu.diplomabackend.model;

import com.sun.istack.Nullable;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "_potential_team")
public class PotentialTeam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "creator_id",nullable = false,insertable = false,updatable = false)
    private User creator;
    @Column(name = "creator_id")
    private Long creatorId;

    @ManyToOne
    @JoinColumn(name = "user_id",nullable = false,insertable = false,updatable = false)
    private User user;
    @Column(name = "user_id")
    private Long userId;

    @Enumerated(EnumType.STRING)
    private TeamMemberRole userRole;

    @Enumerated(EnumType.STRING)
    private Status status;
}
