package kz.iitu.diplomabackend.model;

import lombok.ToString;

@ToString
public enum Status {
    ACCEPTED, REJECTED, UNDEFINED
}
