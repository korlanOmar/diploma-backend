package kz.iitu.diplomabackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "_user_grade")
public class UserGrade {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "user_id",insertable = false,nullable = false,updatable = false)
  private User user;
  @Column(name = "user_id")
  private Long userId;

  private String grade;
}
