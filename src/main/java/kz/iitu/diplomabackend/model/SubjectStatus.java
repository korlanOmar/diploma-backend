package kz.iitu.diplomabackend.model;

import lombok.ToString;
@ToString
public enum SubjectStatus {
    PASSED, FAILED, UNDEFINED
}
