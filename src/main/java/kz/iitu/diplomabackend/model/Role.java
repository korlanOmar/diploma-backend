package kz.iitu.diplomabackend.model;

import lombok.ToString;

@ToString
public enum Role {
  USER,SECRETARIAT
}
