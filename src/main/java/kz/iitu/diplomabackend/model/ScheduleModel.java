package kz.iitu.diplomabackend.model;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class ScheduleModel {
    private Long id;
    private String eventName;
    private Timestamp time;
    private Long teacherId;
    private Long teamId;
    private boolean isDefense;
}
