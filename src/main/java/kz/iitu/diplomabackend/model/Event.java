package kz.iitu.diplomabackend.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity
@Table(name = "_event")
public class Event {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  private Timestamp time;

  @ManyToOne
  @JoinColumn(name = "teacher_id",nullable = false,insertable = false,updatable = false)
  private User user;
  @Column(name = "teacher_id")
  private Long userId;
}
