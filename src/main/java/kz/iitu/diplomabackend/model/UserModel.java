package kz.iitu.diplomabackend.model;

import lombok.Data;

@Data
public class UserModel {
    private int id;
    private String name;
    private String surname;
    private String email;
    private TeamMemberRole role;
}
