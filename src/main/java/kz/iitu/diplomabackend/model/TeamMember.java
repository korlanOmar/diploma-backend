package kz.iitu.diplomabackend.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "_team_member")
public class TeamMember {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "user_id",nullable = false,insertable = false,updatable = false)
  private User user;
  @Column(name = "user_id")
  private Long userId;
  @ManyToOne
  @JoinColumn(name = "team_id",nullable = false,insertable = false,updatable = false)
  private Team team;
  @Column(name = "team_id")
  private Long teamId;

  @Enumerated(EnumType.STRING)
  private TeamMemberRole role;

}
