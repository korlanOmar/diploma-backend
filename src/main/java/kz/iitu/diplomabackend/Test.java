//package kz.iitu.diplomabackend;
//
//import kz.iitu.diplomabackend.config.SpringConfig;
//import kz.iitu.diplomabackend.controller.UserController;
//import kz.iitu.diplomabackend.entity.User;
//import kz.iitu.diplomabackend.repository.UserRepository;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//
//import java.util.Scanner;
//
//public class Test {
//    private static Scanner in;
//    private static UserController userController;
//
//    public static void main (String[]args) {
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
//        in = new Scanner(System.in);
//        userController = context.getBean("userController", UserController.class);
//
//        while (true) {
//            menu();
//        }
//    }
//
//    private static void menu () {
//        System.out.println("1. Sign in\n" +
//                "2. Sign Up\n" +
//                "3. Exit");
//        int selector = in.nextInt();
//        switch (selector) {
//            case 1:
//                loginUser();
//                break;
//            case 2:
//                createUser();
//                break;
//            case 3:
//                System.exit(0);
//        }
//    }
//
//    private static void loginUser() {
//        System.out.println("Log in:\nEnter the login");
//        String login = in.next();
//        System.out.println("Enter the password");
//        String password = in.next();
//        for (User user : userController.getAllUsers()) {
//            if (user.getUser_email().equals(login) && user.getUser_password().equals(password)) {
//                System.out.println("Welcome, " + user.getUser_firstname());
//            }
//        }
//    }
//
//    private static void createUser() {
//        System.out.println("Enter the name");
//        String name = in.next();
//        System.out.println("Enter the email");
//        String login = in.next();
//        if (userController.getAllUsers() != null) {
//            for (User user : userController.getAllUsers()) {
//                if (user.getUser_email().equals(login)) {
//                    System.out.println("Such login already exists");
//                    return;
//                }
//            }
//        }
//        System.out.println("Enter the password");
//        String password = in.next();
//        userController.createUser(new User(name, login, password));
//        loginUser();
//    }
//}
