//package kz.iitu.diplomabackend.config;
//
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//
//@Configuration
//@ComponentScan(basePackages = "kz.iitu.diplomabackend")
//@PropertySource("application.properties")
//public class SpringConfig {
//}
