package kz.iitu.diplomabackend.repository;

import kz.iitu.diplomabackend.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

@Repository
public interface EventRepository extends JpaRepository<Event,Long> {
    Event findEventByNameAndTime(String name, Timestamp time);
}
