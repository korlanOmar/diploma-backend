package kz.iitu.diplomabackend.repository;

import kz.iitu.diplomabackend.model.TeamMember;
import kz.iitu.diplomabackend.model.TeamMemberRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamMemberRepo extends JpaRepository<TeamMember,Long> {
  TeamMember getById(Long id);
  List<TeamMember> getAllByTeamId(Long id);
  List<TeamMember> getAllByTeamIdAndRole(Long id,TeamMemberRole role);
  List<TeamMember> getAllByUserId(Long id);
  List<TeamMember> getAllByRole(TeamMemberRole role);
  void deleteTeamMemberByTeamId(Long teamId);
}
