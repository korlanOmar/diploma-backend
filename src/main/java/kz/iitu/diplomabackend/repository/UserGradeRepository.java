package kz.iitu.diplomabackend.repository;

import kz.iitu.diplomabackend.model.UserGrade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGradeRepository extends JpaRepository<UserGrade, Long>  {
}
