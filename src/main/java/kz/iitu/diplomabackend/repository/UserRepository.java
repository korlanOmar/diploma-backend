package kz.iitu.diplomabackend.repository;

import kz.iitu.diplomabackend.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findUserByUsernameIgnoreCase(String name);
    List<User> findUserByUsernameIs(String username);
    List<User> findUserByPosition(String position);
    Optional<User> findUserByUsername(String username);
}
