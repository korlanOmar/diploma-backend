package kz.iitu.diplomabackend.repository;

import kz.iitu.diplomabackend.model.Defense;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DefenceRepository extends JpaRepository<Defense,Long> {
    List<Defense> findAllByTeamId(Long teamId);
    List<Defense> findAllByTeamIdAndEventId(Long teamId,Long eventId);
}
