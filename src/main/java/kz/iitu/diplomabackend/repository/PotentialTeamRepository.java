package kz.iitu.diplomabackend.repository;

import kz.iitu.diplomabackend.model.PotentialTeam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PotentialTeamRepository extends JpaRepository<PotentialTeam, Long> {
    List<PotentialTeam> findAllByCreatorId(Long creatorId);
    List<PotentialTeam> findAllByUserId(Long userId);
}
