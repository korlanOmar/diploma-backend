package kz.iitu.diplomabackend.repository;

import kz.iitu.diplomabackend.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<Subject,Long> {
    List<Subject> findAllByTeamId(Long teamId);
}
