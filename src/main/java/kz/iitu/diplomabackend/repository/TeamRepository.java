package kz.iitu.diplomabackend.repository;

import kz.iitu.diplomabackend.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
    Team getTeamByName(String name);
}
