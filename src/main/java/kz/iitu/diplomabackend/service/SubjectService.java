package kz.iitu.diplomabackend.service;

import kz.iitu.diplomabackend.model.Subject;
import kz.iitu.diplomabackend.model.SubjectStatus;

import java.util.List;

public interface SubjectService {
    List<Subject> getAll();
    Subject findSubjectById(Long id);
    void createSubject(Long eventId, Long teamId, SubjectStatus status);
    void updateSubject(Long id, Long eventId, Long teamId, SubjectStatus status);
    void deleteSubject(Long id);
    List<Subject> findByTeamId(Long teamId);
}
