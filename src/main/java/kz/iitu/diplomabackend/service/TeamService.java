package kz.iitu.diplomabackend.service;

import kz.iitu.diplomabackend.model.Team;

import java.util.List;
import java.util.Optional;

public interface TeamService {
    List<Team> getAll();
    Optional<Team> findTeamById(Long id);
    void createTeam(Team team);
    void updateTeam(Team team);
    void deleteTeam(Long id);
    Team getTeamByName(String name);
}
