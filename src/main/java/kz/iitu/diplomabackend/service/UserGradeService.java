package kz.iitu.diplomabackend.service;

import kz.iitu.diplomabackend.model.UserGrade;

import java.util.List;
import java.util.Optional;

public interface UserGradeService {
    List<UserGrade> getAll();
    Optional<UserGrade> findUserGradeById(Long id);
    void createUserGrade(Long userId, String grade);
    void updateUserGrade(Long id, Long userId, String grade);
    void deleteUserGrade(Long id);
}
