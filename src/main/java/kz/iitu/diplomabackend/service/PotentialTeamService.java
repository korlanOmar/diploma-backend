package kz.iitu.diplomabackend.service;

import kz.iitu.diplomabackend.model.PotentialTeam;

import java.util.List;
import java.util.Optional;

public interface PotentialTeamService {
    List<PotentialTeam> findAllByCreatorId(Long id);
    List<PotentialTeam> findAllByUserId(Long id);
    Optional<PotentialTeam> findById(Long id);
    void updatePotentialTeam(PotentialTeam potentialTeam);
    List<PotentialTeam> getAll();
    void createPotentialTeam(PotentialTeam potentialTeam);
    void deletePotentialTeam(Long id);
}
