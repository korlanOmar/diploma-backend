package kz.iitu.diplomabackend.service;

import kz.iitu.diplomabackend.model.UserModel;

import java.util.List;

public interface UserUploadService {
    void uploadList(List<UserModel> users);
}
