package kz.iitu.diplomabackend.service;

import kz.iitu.diplomabackend.model.Event;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface EventService {
    List<Event> getAll();
    Optional<Event> findEventById(Long id);
    void createEvent(Event event);
    void updateEvent(Event event);
    void deleteEvent(Long id);
    Event findEventByNameAndTime(String name, Timestamp time);
}
