package kz.iitu.diplomabackend.service;

import kz.iitu.diplomabackend.model.TeamMember;

import java.util.List;
import java.util.Optional;

public interface TeamMemberService {
    List<TeamMember> getAll();
    List<TeamMember> getAllByTeamId(Long id);
    Optional<TeamMember> findTeamMemberById(Long id);
    void createTeamMember(Long userId, Long teamId, String role);
    void updateTeamMember(Long id, Long userId, Long teamId, String role);
    void deleteTeamMember(Long id);
    void deleteTeam(Long teamId);
}
