package kz.iitu.diplomabackend.service.impl;

import kz.iitu.diplomabackend.model.Event;
import kz.iitu.diplomabackend.repository.EventRepository;
import kz.iitu.diplomabackend.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {
    @Autowired
    private EventRepository eventRepository;
    @Override
    public List<Event> getAll() {
        return eventRepository.findAll();
    }

    @Override
    public Optional<Event> findEventById(Long id) {
        return eventRepository.findById(id);
    }

    @Override
    public void createEvent(Event event) {
        eventRepository.save(event);
    }

    @Override
    public void updateEvent(Event event) {
        Optional<Event> event1 = eventRepository.findById(event.getId());
        event1.orElseThrow().setName(event.getName());
        event1.orElseThrow().setTime(event.getTime());
        eventRepository.save(event1.orElseThrow());
    }

    @Override
    public void deleteEvent(Long id) {
        eventRepository.deleteById(id);
    }

    @Override
    public Event findEventByNameAndTime(String name, Timestamp time) {
        return eventRepository.findEventByNameAndTime(name, time);
    }
}
