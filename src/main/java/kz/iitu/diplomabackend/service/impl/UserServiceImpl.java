package kz.iitu.diplomabackend.service.impl;

import kz.iitu.diplomabackend.model.User;
import kz.iitu.diplomabackend.repository.UserRepository;
import kz.iitu.diplomabackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Transactional
    public List<User> findAllUsers() { return (List<User>) userRepository.findAll(); }

    @Transactional
    public User findUserById(Long id) {
        return userRepository.findById(id).get();
    }

    @Transactional
    public void createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Transactional
    public void updateUser(User user) {
        userRepository.save(user);
    }

    @Transactional
    public void deleteUser(Long id) { userRepository.deleteById(id); }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByUsernameIgnoreCase(username);
        if(user == null){
            throw new UsernameNotFoundException("User: " +username+" has not found");
        }
        return user;
    }

    @Override
    public List<User> getUsersByUsername(String username) {
        return userRepository.findUserByUsernameIs(username);
    }

    @Override
    public List<User> getUsersByPosition(String position) {
        return userRepository.findUserByPosition(position);
    }

    @Override
    public Optional<User> findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }
}
