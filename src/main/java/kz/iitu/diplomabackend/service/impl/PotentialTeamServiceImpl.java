package kz.iitu.diplomabackend.service.impl;

import kz.iitu.diplomabackend.model.PotentialTeam;
import kz.iitu.diplomabackend.repository.PotentialTeamRepository;
import kz.iitu.diplomabackend.service.PotentialTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PotentialTeamServiceImpl implements PotentialTeamService {

    @Autowired
    private PotentialTeamRepository potentialTeamRepository;

    @Override
    public List<PotentialTeam> findAllByCreatorId(Long id) {
        return potentialTeamRepository.findAllByCreatorId(id);
    }

    @Override
    public List<PotentialTeam> findAllByUserId(Long id) {
        return potentialTeamRepository.findAllByUserId(id);
    }

    @Override
    public Optional<PotentialTeam> findById(Long id) {
        return potentialTeamRepository.findById(id);
    }

    @Override
    public List<PotentialTeam> getAll() {
        return potentialTeamRepository.findAll();
    }

    @Override
    public void createPotentialTeam(PotentialTeam potentialTeam) {
        potentialTeamRepository.save(potentialTeam);
    }

    @Override
    public void deletePotentialTeam(Long id) {
        potentialTeamRepository.deleteById(id);
    }

    @Transactional
    public void updatePotentialTeam(PotentialTeam potentialTeam) {
        potentialTeamRepository.save(potentialTeam);
    }
}
