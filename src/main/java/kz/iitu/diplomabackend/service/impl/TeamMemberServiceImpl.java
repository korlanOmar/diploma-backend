package kz.iitu.diplomabackend.service.impl;

import kz.iitu.diplomabackend.model.Team;
import kz.iitu.diplomabackend.model.TeamMember;
import kz.iitu.diplomabackend.model.TeamMemberRole;
import kz.iitu.diplomabackend.model.User;
import kz.iitu.diplomabackend.repository.TeamMemberRepo;
import kz.iitu.diplomabackend.repository.TeamRepository;
import kz.iitu.diplomabackend.repository.UserRepository;
import kz.iitu.diplomabackend.service.TeamMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeamMemberServiceImpl implements TeamMemberService {
    @Autowired
    private TeamMemberRepo teamMemberRepo;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<TeamMember> getAll(){
        return teamMemberRepo.findAll();
    }

    @Override
    public List<TeamMember> getAllByTeamId(Long id) {
        return teamMemberRepo.getAllByTeamId(id);
    }

    @Override
    public Optional<TeamMember> findTeamMemberById(Long id) {
        return teamMemberRepo.findById(id);
    }

    @Override
    public void createTeamMember(Long userId, Long teamId, String role) {
        Optional<User> user = userRepository.findById(userId);
        Optional<Team> team = teamRepository.findById(teamId);
        TeamMember teamMember = new TeamMember();
        TeamMemberRole teamMemberRole = TeamMemberRole.STUDENT;
        if (role.equals(TeamMemberRole.COMMISSION.toString())) {
            teamMemberRole = TeamMemberRole.COMMISSION;
        } else if (role.equals(TeamMemberRole.SV.toString())) {
            teamMemberRole = TeamMemberRole.SV;
        } else if (role.equals(TeamMemberRole.TEACHER.toString())) {
            teamMemberRole = TeamMemberRole.TEACHER;
        }
        teamMember.setRole(teamMemberRole);
        teamMember.setTeam(team.orElseThrow());
        teamMember.setTeamId(teamId);
        teamMember.setUser(user.orElseThrow());
        teamMember.setUserId(userId);
        teamMemberRepo.save(teamMember);
    }

    @Override
    public void updateTeamMember(Long id, Long userId, Long teamId, String role) {
        Optional<TeamMember> teamMember = teamMemberRepo.findById(id);
        Optional<User> user = userRepository.findById(userId);
        Optional<Team> team = teamRepository.findById(teamId);
        TeamMemberRole teamMemberRole = TeamMemberRole.STUDENT;
        if (role.equals(TeamMemberRole.COMMISSION.toString())) {
            teamMemberRole = TeamMemberRole.COMMISSION;
        } else if (role.equals(TeamMemberRole.SV.toString())) {
            teamMemberRole = TeamMemberRole.SV;
        } else if (role.equals(TeamMemberRole.TEACHER.toString())) {
            teamMemberRole = TeamMemberRole.TEACHER;
        }
        teamMember.orElseThrow().setRole(teamMemberRole);
        teamMember.orElseThrow().setTeam(team.orElseThrow());
        teamMember.orElseThrow().setTeamId(teamId);
        teamMember.orElseThrow().setUser(user.orElseThrow());
        teamMember.orElseThrow().setUserId(userId);
        teamMemberRepo.save(teamMember.orElseThrow());
    }

    @Override
    public void deleteTeamMember(Long id) {
        teamMemberRepo.deleteById(id);
    }

    @Override
    public void deleteTeam(Long teamId) {
        teamMemberRepo.deleteTeamMemberByTeamId(teamId);
    }
}
