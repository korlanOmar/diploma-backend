package kz.iitu.diplomabackend.service.impl;

import kz.iitu.diplomabackend.model.*;
import kz.iitu.diplomabackend.repository.EventRepository;
import kz.iitu.diplomabackend.repository.SubjectRepository;
import kz.iitu.diplomabackend.repository.TeamRepository;
import kz.iitu.diplomabackend.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SubjectServiceImpl implements SubjectService {
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private TeamRepository teamRepository;

    @Override
    public List<Subject> getAll() {
        return subjectRepository.findAll();
    }

    @Override
    public Subject findSubjectById(Long id) {
        return subjectRepository.findById(id).get();
    }

    @Override
    public void createSubject(Long eventId, Long teamId, SubjectStatus status) {
        Optional<Event> event = eventRepository.findById(eventId);
        Optional<Team> team = teamRepository.findById(teamId);
        Subject subject = new Subject();
        subject.setEventId(eventId);
        subject.setEvent(event.orElseThrow());
        subject.setTeamId(teamId);
        subject.setTeam(team.orElseThrow());
        subject.setStatus(status);
        subjectRepository.save(subject);
    }

    @Override
    public void updateSubject(Long id, Long eventId, Long teamId, SubjectStatus status) {
        Optional<Subject> subject = subjectRepository.findById(id);
        Optional<Event> event = eventRepository.findById(eventId);
        Optional<Team> team = teamRepository.findById(teamId);
        subject.orElseThrow().setEventId(eventId);
        subject.orElseThrow().setEvent(event.orElseThrow());
        subject.orElseThrow().setTeamId(teamId);
        subject.orElseThrow().setTeam(team.orElseThrow());
        subject.orElseThrow().setStatus(status);
        subjectRepository.save(subject.orElseThrow());
    }

    @Override
    public List<Subject> findByTeamId(Long teamId) {
        return subjectRepository.findAllByTeamId(teamId);
    }

    @Override
    public void deleteSubject(Long id) {
        subjectRepository.deleteById(id);
    }
}
