package kz.iitu.diplomabackend.service.impl;

import kz.iitu.diplomabackend.model.*;
import kz.iitu.diplomabackend.repository.DefenceRepository;
import kz.iitu.diplomabackend.repository.EventRepository;
import kz.iitu.diplomabackend.repository.TeamRepository;
import kz.iitu.diplomabackend.repository.UserGradeRepository;
import kz.iitu.diplomabackend.service.DefenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DefenceServiceImpl implements DefenceService {
    @Autowired
    private DefenceRepository defenceRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private UserGradeRepository userGradeRepository;

    @Override
    public List<Defense> getAll() {
        return defenceRepository.findAll();
    }

    @Override
    public Optional<Defense> findDefenceById(Long id) {
        return defenceRepository.findById(id);
    }

    @Override
    public void createDefence(Long eventId, Long teamId, Long userGradeId) {
        Optional<Event> event = eventRepository.findById(eventId);
        Optional<Team> team = teamRepository.findById(teamId);
        Optional<UserGrade> userGrade = userGradeRepository.findById(userGradeId);
        Defense defense = new Defense();
        defense.setEventId(eventId);
        defense.setEvent(event.orElseThrow());
        defense.setTeamId(teamId);
        defense.setTeam(team.orElseThrow());
        defense.setUserGradeId(userGradeId);
        defense.setUserGrade(userGrade.orElseThrow());
        defenceRepository.save(defense);
    }

    @Override
    public List<Defense> findByTeamId(Long teamId) {
        return defenceRepository.findAllByTeamId(teamId);
    }

    @Override
    public void updateDefence(Long id, Long eventId, Long teamId, Long userGradeId) {
        Optional<Defense> defense = defenceRepository.findById(id);
        Optional<Event> event = eventRepository.findById(eventId);
        Optional<Team> team = teamRepository.findById(teamId);
        Optional<UserGrade> userGrade = userGradeRepository.findById(userGradeId);
        defense.orElseThrow().setEventId(eventId);
        defense.orElseThrow().setEvent(event.orElseThrow());
        defense.orElseThrow().setTeamId(teamId);
        defense.orElseThrow().setTeam(team.orElseThrow());
        defense.orElseThrow().setUserGradeId(userGradeId);
        defense.orElseThrow().setUserGrade(userGrade.orElseThrow());
        defenceRepository.save(defense.orElseThrow());
    }

    @Override
    public void deleteDefence(Long id) {
        defenceRepository.deleteById(id);
    }
}
