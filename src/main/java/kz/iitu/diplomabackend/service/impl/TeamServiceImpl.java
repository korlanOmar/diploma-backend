package kz.iitu.diplomabackend.service.impl;

import kz.iitu.diplomabackend.model.Team;
import kz.iitu.diplomabackend.repository.TeamRepository;
import kz.iitu.diplomabackend.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeamServiceImpl implements TeamService {
    @Autowired
    private TeamRepository teamRepository;

    public List<Team> getAll(){
        return teamRepository.findAll();
    }

    @Override
    public Optional<Team> findTeamById(Long id) {
        return teamRepository.findById(id);
    }

    @Override
    public void createTeam(Team team) {
        teamRepository.save(team);
    }

    @Override
    public void updateTeam(Team team) {
        Optional<Team> team1 = teamRepository.findById(team.getId());
        team1.orElseThrow().setName(team.getName());
        team1.orElseThrow().setTheme(team.getTheme());
        teamRepository.save(team1.orElseThrow());
    }

    @Override
    public void deleteTeam(Long id) {
       teamRepository.deleteById(id);
    }

    @Override
    public Team getTeamByName(String name) {
        return teamRepository.getTeamByName(name);
    }
}
