package kz.iitu.diplomabackend.service.impl;

import kz.iitu.diplomabackend.model.User;
import kz.iitu.diplomabackend.model.UserGrade;
import kz.iitu.diplomabackend.repository.UserGradeRepository;
import kz.iitu.diplomabackend.repository.UserRepository;
import kz.iitu.diplomabackend.service.UserGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserGradeServiceImpl implements UserGradeService {
    @Autowired
    private UserGradeRepository userGradeRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<UserGrade> getAll() {
        return userGradeRepository.findAll();
    }

    @Override
    public Optional<UserGrade> findUserGradeById(Long id) {
        return userGradeRepository.findById(id);
    }

    @Override
    public void createUserGrade(Long userId, String grade) {
        Optional<User> user = userRepository.findById(userId);
        UserGrade userGrade = new UserGrade();
        userGrade.setUserId(userId);
        userGrade.setUser(user.orElseThrow());
        userGrade.setGrade(grade);
        userGradeRepository.save(userGrade);
    }

    @Override
    public void updateUserGrade(Long id, Long userId, String grade) {
        Optional<UserGrade> userGrade = userGradeRepository.findById(id);
        Optional<User> user = userRepository.findById(userId);
        userGrade.orElseThrow().setUserId(userId);
        userGrade.orElseThrow().setUser(user.orElseThrow());
        userGrade.orElseThrow().setGrade(grade);
        userGradeRepository.save(userGrade.orElseThrow());
    }

    @Override
    public void deleteUserGrade(Long id) {
        userGradeRepository.deleteById(id);
    }
}
