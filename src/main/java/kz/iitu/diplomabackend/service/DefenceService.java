package kz.iitu.diplomabackend.service;

import kz.iitu.diplomabackend.model.Defense;

import java.util.List;
import java.util.Optional;

public interface DefenceService {
    List<Defense> getAll();
    Optional<Defense> findDefenceById(Long id);
    void createDefence(Long eventId, Long teamId, Long userGradeId);
    void updateDefence(Long id, Long eventId, Long teamId, Long userGradeId);
    void deleteDefence(Long id);
    List<Defense> findByTeamId(Long teamId);
}
