package kz.iitu.diplomabackend.service;

import kz.iitu.diplomabackend.model.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> findAllUsers();
    User findUserById(Long id);
    void createUser(User user);
    void updateUser(User user);
    void deleteUser(Long id);
    UserDetails loadUserByUsername(String username);
    List<User> getUsersByUsername(String username);
    List<User> getUsersByPosition(String position);
    Optional<User> findUserByUsername(String username);
}
